<?php

declare(strict_types=1);

namespace Tests\Smorken\SocialAuth\Unit\Admin\Support\MsGraph;

use Microsoft\Graph\Beta\Generated\Models\OnPremisesExtensionAttributes;
use Microsoft\Graph\Beta\Generated\Models\User as GraphUser;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\SocialAuth\Admin\External\Support\MsGraph\UserToSocialiteUser;

class UserToSocialiteUserTest extends TestCase
{
    #[Test]
    public function it_converts_graph_user_to_array(): void
    {
        $user = new GraphUser;
        $user->setId('1234-abc');
        $user->setGivenName('John');
        $user->setSurname('Doe');
        $user->setDisplayName('Doe, John');
        $user->setMail('john.doe@example.edu');
        $user->setUserPrincipalName('abc1234567@example.edu');
        $attributes = new OnPremisesExtensionAttributes;
        $attributes->setExtensionAttribute12('39999999');
        $attributes->setExtensionAttribute14('john.doe@example.edu');
        $user->setOnPremisesExtensionAttributes($attributes);
        $arr = UserToSocialiteUser::toArray($user);
        $this->assertEquals([
            'additionalData' => [],
            'odataType' => '#microsoft.graph.user',
            'id' => '1234-abc',
            'givenName' => 'John',
            'surname' => 'Doe',
            'displayName' => 'Doe, John',
            'mail' => 'john.doe@example.edu',
            'userPrincipalName' => 'abc1234567@example.edu',
            'onPremisesExtensionAttributes' => [
                'additionalData' => [],
                'extensionAttribute12' => '39999999',
                'extensionAttribute14' => 'john.doe@example.edu',
            ],
        ], $arr);
    }

    #[Test]
    public function it_converts_graph_user_to_socialite_user(): void
    {
        $user = new GraphUser;
        $user->setId('1234-abc');
        $user->setGivenName('John');
        $user->setSurname('Doe');
        $user->setDisplayName('Doe, John');
        $user->setMail('john.doe@example.edu');
        $user->setUserPrincipalName('abc1234567@example.edu');
        $attributes = new OnPremisesExtensionAttributes;
        $attributes->setExtensionAttribute12('39999999');
        $attributes->setExtensionAttribute14('john.doe@example.edu');
        $user->setOnPremisesExtensionAttributes($attributes);
        $socialiteUser = UserToSocialiteUser::convert($user);
        $this->assertEquals([
            'id' => '1234-abc',
            'nickname' => null,
            'name' => 'Doe, John',
            'email' => 'abc1234567@example.edu',
            'avatar' => null,
        ], $socialiteUser->attributes);
        $this->assertEquals([
            'additionalData' => [],
            'extensionAttribute12' => '39999999',
            'extensionAttribute14' => 'john.doe@example.edu',
        ], $socialiteUser->getRaw()['onPremisesExtensionAttributes']);
    }
}
