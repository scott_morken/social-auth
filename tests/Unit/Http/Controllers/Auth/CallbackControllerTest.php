<?php

declare(strict_types=1);

namespace Tests\Smorken\SocialAuth\Unit\Http\Controllers\Auth;

use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Contracts\Provider;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\SocialAuth\Callback\Actions\UpsertEloquentUserAction;
use Smorken\SocialAuth\Callback\Attributes\AttributeResolver;
use Smorken\SocialAuth\Callback\Attributes\Maps\AzureMccdMap;
use Smorken\SocialAuth\Callback\Validation\ValidateMeidInUpn;
use Smorken\SocialAuth\Contracts\SocialFactory;
use Smorken\SocialAuth\Events\LoggedIn;
use Smorken\SocialAuth\Http\Controllers\Auth\CallbackController;
use Tests\Smorken\SocialAuth\Concerns\WithMockConnection;

class CallbackControllerTest extends TestCase
{
    use WithMockConnection;

    protected ?Factory $authFactory = null;

    protected ?Dispatcher $eventDispatcher = null;

    protected ?ExceptionHandler $exceptionHandler = null;

    protected ?SocialFactory $socialFactory = null;

    protected ?Provider $socialiteProvider = null;

    protected ?\Smorken\Auth\Contracts\Models\User $userModel = null;

    #[Test]
    public function it_creates_a_new_user(): void
    {
        $raw = file_get_contents(__DIR__.'/../../../../data/entra.me.json');
        $this->getSocialiteProvider()->expects()->user()->andReturn($this->mapToUser(json_decode($raw, true)));
        $query = m::mock(Builder::class);
        $this->getUserModel()->expects()->newQuery()->andReturn($query);
        $query->expects()->where('username', '=', 'abc1234567');
        $query->expects()->first()->andReturn(null);
        $m = m::mock(new User)->makePartial();
        $this->getUserModel()->expects()->newInstance()->andReturns($m);
        $m->expects()->save()->andReturn(true);
        $this->getEventDispatcher()->expects()
            ->dispatch(m::type(LoggedIn::class));
        $this->getAuthFactory()->shouldReceive('guard->login')->once()->with($m);
        $redirect = Redirect::partialMock();
        $redirectResponse = new RedirectResponse('/');
        $redirect->expects()->intended('/')->andReturns($redirectResponse);
        $sut = $this->getSut();
        $response = $sut(new Request);
        $this->assertEquals($redirectResponse, $response);
    }

    #[Test]
    public function it_fails_with_an_invalid_meid_upn(): void
    {
        $raw = file_get_contents(__DIR__.'/../../../../data/entra.me.invalidMeid.json');
        $this->getSocialiteProvider()->expects()->user()->andReturn($this->mapToUser(json_decode($raw, true)));
        $this->getAuthFactory()->shouldReceive('guard->login')->never();
        $redirect = Redirect::partialMock();
        $redirectResponse = new RedirectResponse('/');
        $redirect->expects()->route('login')->andReturns($redirectResponse);
        $sut = $this->getSut();
        $request = m::mock(Request::class);
        $request->allows()->wantsJson()->andReturn(false);
        $request->expects()->session()->andReturnSelf();
        $request->expects()
            ->flash('flash:danger:0',
                "Your selected account does not match 'meid@maricopa.edu'.  You provided 'notanmeid@example.edu'.");
        $response = $sut($request);
        $this->assertEquals($redirectResponse, $response);
    }

    #[Test]
    public function it_find_an_existing_user(): void
    {
        $raw = file_get_contents(__DIR__.'/../../../../data/entra.me.json');
        $this->getSocialiteProvider()->expects()->user()->andReturn($this->mapToUser(json_decode($raw, true)));
        $query = m::mock(Builder::class);
        $this->getUserModel()->expects()->newQuery()->andReturn($query);
        $user = m::mock(new User([
            'id' => '39999999',
            'username' => 'abc1234567',
            'first_name' => 'Foo',
            'last_name' => 'Bar',
            'email' => 'foo.bar@example.edu',
        ]))->makePartial();
        $user->expects()->save()->never();
        $query->expects()->where('username', '=', 'abc1234567');
        $query->expects()->first()->andReturn($user);
        $this->getEventDispatcher()->expects()
            ->dispatch(m::type(LoggedIn::class));
        $this->getAuthFactory()->shouldReceive('guard->login')->once()->with($user);
        $redirect = Redirect::partialMock();
        $redirectResponse = new RedirectResponse('/');
        $redirect->expects()->intended('/')->andReturns($redirectResponse);
        $sut = $this->getSut();
        $response = $sut(new Request);
        $this->assertEquals($redirectResponse, $response);
    }

    #[Test]
    public function it_updates_an_existing_user(): void
    {
        $raw = file_get_contents(__DIR__.'/../../../../data/entra.me.json');
        $this->getSocialiteProvider()->expects()->user()->andReturn($this->mapToUser(json_decode($raw, true)));
        $query = m::mock(Builder::class);
        $this->getUserModel()->expects()->newQuery()->andReturn($query);
        $user = m::mock(new User([
            'id' => '39999999',
            'username' => 'abc1234567',
            'first_name' => 'Nope',
            'last_name' => 'NotSame',
            'email' => 'not.same@example.edu',
        ]))->makePartial();
        $user->expects()->save()->andReturns(true);
        $query->expects()->where('username', '=', 'abc1234567');
        $query->expects()->first()->andReturn($user);
        $this->getEventDispatcher()->expects()
            ->dispatch(m::type(LoggedIn::class));
        $this->getAuthFactory()->shouldReceive('guard->login')->once()->with($user);
        $redirect = Redirect::partialMock();
        $redirectResponse = new RedirectResponse('/');
        $redirect->expects()->intended('/')->andReturns($redirectResponse);
        $sut = $this->getSut();
        $response = $sut(new Request);
        $this->assertEquals($redirectResponse, $response);
    }

    protected function getAuthFactory(): Factory
    {
        if ($this->authFactory === null) {
            $this->authFactory = m::mock(Factory::class);
        }

        return $this->authFactory;
    }

    protected function getEventDispatcher(): Dispatcher
    {
        return $this->eventDispatcher ??= m::mock(Dispatcher::class);
    }

    protected function getExceptionHandler(): ExceptionHandler
    {
        if ($this->exceptionHandler === null) {
            $this->exceptionHandler = m::mock(ExceptionHandler::class);
        }

        return $this->exceptionHandler;
    }

    protected function getSocialFactory(): SocialFactory
    {
        if ($this->socialFactory === null) {
            $this->socialFactory = m::mock(SocialFactory::class);
            $this->socialFactory->allows()->provider()->andReturns($this->getSocialiteProvider());
        }

        return $this->socialFactory;
    }

    protected function getSocialiteProvider(): Provider
    {
        if ($this->socialiteProvider === null) {
            $this->socialiteProvider = m::mock(Provider::class);
            $this->socialiteProvider->allows()->stateless()->andReturnSelf();
        }

        return $this->socialiteProvider;
    }

    protected function getSut(): CallbackController
    {
        AttributeResolver::setAttributesClass(AzureMccdMap::class);

        return new CallbackController(
            $this->getSocialFactory(),
            $this->getAuthFactory(),
            new UpsertEloquentUserAction(new AttributeResolver, $this->getUserModel()),
            new ValidateMeidInUpn,
            $this->getExceptionHandler()
        );
    }

    protected function getUserModel(): \Smorken\Auth\Contracts\Models\User
    {
        if ($this->userModel === null) {
            $this->userModel = m::mock(new \Smorken\Auth\Models\Eloquent\User)->makePartial();
        }

        return $this->userModel;
    }

    protected function initEventDispatcher(): void
    {
        $c = Container::getInstance();
        $c->bind('events', fn () => $this->getEventDispatcher());
    }

    protected function mapToUser(array $user): \Laravel\Socialite\Contracts\User
    {
        return (new \SocialiteProviders\Manager\OAuth2\User)->setRaw($user)->map([
            'id' => $user['id'],
            'nickname' => null,
            'name' => $user['displayName'],
            'email' => $user['userPrincipalName'],
            'avatar' => null,
        ]);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
        $this->initEventDispatcher();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
