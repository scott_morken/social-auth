<?php

declare(strict_types=1);

namespace Tests\Smorken\SocialAuth\Unit\Callback\Actions;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use Microsoft\Graph\Beta\Generated\Models\OnPremisesExtensionAttributes;
use Microsoft\Graph\Beta\Generated\Models\User as GraphUser;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\Contracts\Models\User;
use Smorken\SocialAuth\Admin\External\Support\MsGraph\UserToSocialiteUser;
use Smorken\SocialAuth\Callback\Actions\UpsertEloquentUserAction;
use Smorken\SocialAuth\Callback\Attributes\AttributeResolver;
use Smorken\SocialAuth\Callback\Attributes\Maps\AzureMccdMap;

class UpsertUserActionTest extends TestCase
{
    #[Test]
    public function it_creates_a_new_user(): void
    {
        $model = m::mock(User::class);
        AttributeResolver::setAttributesClass(AzureMccdMap::class);
        $query = m::mock(Builder::class);
        $model->expects()->newQuery()->andReturn($query);
        $query->expects()->where('username', '=', 'abc1234567')->andReturnSelf();
        $query->expects()->first()->andReturn(null);
        $user = m::mock(User::class);
        $model->expects()->newInstance()->andReturn($user);
        $user->expects()->fill([
            'username' => 'abc1234567', 'id' => '39999999', 'first_name' => 'John', 'last_name' => 'Doe',
            'email' => 'john.doe@example.edu',
        ]);
        $user->expects()->save()->andReturn(true);
        $sut = new UpsertEloquentUserAction(new AttributeResolver, $model);
        $u = $sut($this->getSocialiteUserFromGraphUser());
        $this->assertSame($user, $u);
    }

    #[Test]
    public function it_updates_a_user(): void
    {
        $model = m::mock(User::class);
        AttributeResolver::setAttributesClass(AzureMccdMap::class);
        $query = m::mock(Builder::class);
        $model->expects()->newQuery()->andReturn($query);
        $query->expects()->where('username', '=', 'abc1234567')->andReturnSelf();
        $user = m::mock(new \Smorken\Auth\Models\Eloquent\User)->makePartial();
        $query->expects()->first()->andReturn($user);
        $user->expects()->save()->andReturn(true);
        $sut = new UpsertEloquentUserAction(new AttributeResolver, $model);
        $u = $sut($this->getSocialiteUserFromGraphUser());
        $this->assertSame($user, $u);
    }

    protected function getSocialiteUserFromGraphUser(): SocialiteUser
    {
        $user = new GraphUser;
        $user->setId('1234-abc');
        $user->setGivenName('John');
        $user->setSurname('Doe');
        $user->setDisplayName('Doe, John');
        $user->setMail('john.doe@example.edu');
        $user->setUserPrincipalName('abc1234567@example.edu');
        $attributes = new OnPremisesExtensionAttributes;
        $attributes->setExtensionAttribute12('39999999');
        $attributes->setExtensionAttribute14('john.doe@example.edu');
        $user->setOnPremisesExtensionAttributes($attributes);

        return UserToSocialiteUser::convert($user);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
