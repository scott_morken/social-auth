<?php

declare(strict_types=1);

namespace Tests\Smorken\SocialAuth\Unit\Callback\Validation;

use Laravel\Socialite\Two\User;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\SocialAuth\Callback\Validation\ValidateMeidInUpn;

class ValidateMeidInUpnTest extends TestCase
{
    #[Test]
    public function it_is_true_with_a_3_7_meid(): void
    {
        $sut = new ValidateMeidInUpn;
        $userArray = ['id' => 'abc123', 'displayName' => 'Foo Bar', 'userPrincipalName' => 'abc1234567@maricopa.edu'];
        $this->assertTrue($sut->validate((new User)->setRaw($userArray)->map([
            'id' => $userArray['id'],
            'nickname' => null,
            'name' => $userArray['displayName'],
            'email' => $userArray['userPrincipalName'],
            'avatar' => null,
        ])));
    }

    #[Test]
    public function it_is_true_with_a_5_5_meid(): void
    {
        $sut = new ValidateMeidInUpn;
        $userArray = ['id' => 'abc123', 'displayName' => 'Foo Bar', 'userPrincipalName' => 'abcde12345@maricopa.edu'];
        $this->assertTrue($sut->validate((new User)->setRaw($userArray)->map([
            'id' => $userArray['id'],
            'nickname' => null,
            'name' => $userArray['displayName'],
            'email' => $userArray['userPrincipalName'],
            'avatar' => null,
        ])));
    }

    #[Test]
    public function it_is_true_with_an_invalid_meid(): void
    {
        $sut = new ValidateMeidInUpn;
        $userArray = ['id' => 'abc123', 'displayName' => 'Foo Bar', 'userPrincipalName' => 'abce1234@maricopa.edu'];
        $this->assertFalse($sut->validate((new User)->setRaw($userArray)->map([
            'id' => $userArray['id'],
            'nickname' => null,
            'name' => $userArray['displayName'],
            'email' => $userArray['userPrincipalName'],
            'avatar' => null,
        ])));
        $this->assertEquals("Your selected account does not match 'meid@maricopa.edu'.  You provided 'abce1234@maricopa.edu'.",
            $sut->getMessage());
    }
}
