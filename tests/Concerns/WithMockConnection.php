<?php

declare(strict_types=1);

namespace Tests\Smorken\SocialAuth\Concerns;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\MySqlConnection;
use Mockery as m;
use Tests\App\Stubs\MockPDO;

trait WithMockConnection
{
    protected \PDOStatement|m\MockInterface|null $statement = null;

    protected function getMockConnection(
        string $connectionClass,
        array $config = [],
        array $methods = []
    ): ConnectionInterface {
        $this->statement = null;
        $this->statement = m::mock(\PDOStatement::class);
        $this->statement->shouldReceive('setFetchMode');
        $pdo = m::mock(MockPDO::class);
        $connection = $this->getMockBuilder($connectionClass)
            ->onlyMethods($methods)
            ->setConstructorArgs([$pdo, '', '', $config])
            ->getMock();
        $connection->enableQueryLog();

        return $connection;
    }

    protected function initConnectionResolver(
        string $name = 'db',
        string $connectionClass = MySqlConnection::class
    ): void {
        $cr = new ConnectionResolver([$name => $this->getMockConnection($connectionClass)]);
        $cr->setDefaultConnection($name);
        Model::setConnectionResolver($cr);
    }
}
