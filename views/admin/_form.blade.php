<?php
use Smorken\Components\Helpers\Model;

/**
 * @var \Smorken\SocialAuth\Shared\Contracts\Models\User $model
 */
?>
@if ($type === 'create')
    <x-smc::input.group>
        @php($m = Model::newInstance($model, 'id'))
        <x-smc::input.label :model="$m">ID</x-smc::input.label>
        <x-smc::input :model="$m"></x-smc::input>
    </x-smc::input.group>
@endif
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'username'))
    <x-smc::input.label :model="$m">MEID</x-smc::input.label>
    <x-smc::input :model="$m"></x-smc::input>
</x-smc::input.group>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'first_name'))
    <x-smc::input.label :model="$m">First Name</x-smc::input.label>
    <x-smc::input :model="$m"></x-smc::input>
</x-smc::input.group>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'last_name'))
    <x-smc::input.label :model="$m">Last Name</x-smc::input.label>
    <x-smc::input :model="$m"></x-smc::input>
</x-smc::input.group>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'email'))
    <x-smc::input.label :model="$m">Email</x-smc::input.label>
    <x-smc::input :model="$m"></x-smc::input>
</x-smc::input.group>
@include('sm-social-auth::admin._role')


