@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-social-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.create
        title="Users Administration"
        form-view="sm-social-auth::admin._form"
        :viewModel="$viewModel"></x-smc::resource.create>
</x-dynamic-component>
