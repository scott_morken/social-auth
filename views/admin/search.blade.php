<?php
/**
 * @var \Smorken\Domain\ViewModels\Contracts\FilteredViewModel $viewModel
 * @var \Smorken\SocialAuth\Admin\External\ValueObjects\UserVO $model
 */
use Illuminate\Support\Str;

$filter = $viewModel->filter();
$layoutComponent = \Illuminate\Support\Facades\Config::get('sm-social-auth.layout', 'layouts.app')
?>
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.title-back>Users Administration</x-smc::resource.title-back>
    @include('sm-social-auth::admin._search_filter_form')
    @if ($viewModel->models() && count($viewModel->models()))
        <x-smc::table>
            <x-slot:head>
                <x-smc::table.heading>External ID</x-smc::table.heading>
                <x-smc::table.heading>Username</x-smc::table.heading>
                <x-smc::table.heading>Name</x-smc::table.heading>
                <x-smc::table.heading>Email</x-smc::table.heading>
            </x-slot:head>
            <x-slot:body>
                @foreach ($viewModel->models() as $model)
                    @php($params = [...['id' => $model->externalId], ...$filter->toArray()])
                    <x-smc::table.row :model="$model">
                        <x-smc::table.cell>
                            <x-smc::link
                                    :href="action([$controller, 'provision'], $params)"
                                    title="Provision {{ $model->externalId }}">{{ Str::limit($model->externalId, 20) }}</x-smc::link>
                        </x-smc::table.cell>
                        <x-smc::table.cell>{{ $model->username }}</x-smc::table.cell>
                        <x-smc::table.cell>{{ $model->first_name }} {{ $model->last_name }}</x-smc::table.cell>
                        <x-smc::table.cell>{{ $model->email }}</x-smc::table.cell>
                    </x-smc::table.row>
                @endforeach
            </x-slot:body>
        </x-smc::table>
    @else
        <div class="text-muted">No records found.</div>
    @endif
</x-dynamic-component>
