<?php
use Illuminate\Support\Facades\Auth;

/**
 * @var \Smorken\SocialAuth\Shared\Contracts\Models\User $model
 * @var \Smorken\Roles\Contracts\Role $roleHandler
 * @var \Smorken\Roles\Contracts\Models\Role $role
 */

$roleHandler = $roleHandler ?? null;
$modelRole = $model->role ?? null;
?>
@if ($roleHandler)
    <div class="body mt-2 mb-2 pt-2 pb-2 border-top border-bottom">
        <div class="row mb-2">
            <div class="col-2">Roles
                @if ($model->hasRole($roleHandler->max()))
                    <span class="text-primary">[SA]</span>
                @endif
            </div>
            <div class="col">
                @foreach($roleHandler->all() as $role)
                    @php($checked = ($modelRole && $modelRole->id === $role->id))
                    @if ($roleHandler->has($role, (int) Auth::id()))
                        <div class="form-check">
                            <label for="role-{{ $role->id }}" class="form-check-label">
                                <input type="radio" name="role" value="{{ $role->id }}"
                                       class="form-check-input"
                                       id="role-{{ $role->id }}" {{ $checked ? 'checked' : null }}>
                                {{ $role->descr }}
                            </label>
                        </div>
                    @else
                        <div class="form-check">
                            <span class="">
                                @if ($checked)
                                    <x-smc::icon.check class="text-success"/><span class="visually-hidden">has</span>
                                @else
                                    <x-smc::icon.x class="text-danger"/><span
                                            class="visually-hidden">does not have</span>
                                @endif
                                {{ $role->descr }}
                            </span>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@else
    <div class="text-danger">Role handler not found.</div>
@endif
