@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-social-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.edit
        title="Users Administration"
        form-view="sm-social-auth::admin._form"
        :viewModel="$viewModel"></x-smc::resource.edit>
</x-dynamic-component>
