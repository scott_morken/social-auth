@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-social-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.show-rows title="Users Administration"
                               :limitAttributes="['id', 'username', 'first_name', 'last_name', 'email', 'created_at', 'updated_at']"
                               :viewModel="$viewModel"></x-smc::resource.show-rows>
    @include('sm-social-auth::admin._role_show', ['model' => $viewModel->model()])
</x-dynamic-component>
