@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-social-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.confirm-delete title="Users Administration"
                          :viewModel="$viewModel"></x-smc::resource.confirm-delete>
</x-dynamic-component>
