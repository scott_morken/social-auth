<?php
/**
 * @var \Smorken\Components\Contracts\Helpers\FormQsFilter $helper
 */
?>
<x-smc::form.qs-filter :filter="$filter">
    @php($helper = $component->helper)
    <x-smc::flex class="justify-content-start">
        <x-smc::input.group>
            <x-smc::input.label for="filter-username" :visible="false">Username (MEID)</x-smc::input.label>
            <x-smc::input name="filter[username]" class="{{ $helper->getClasses('username') }}"
                          :value="$helper->getValue('username')" placeholder="MEID"/>
        </x-smc::input.group>
        <x-smc::input.group>
            <x-smc::input.label for="filter-firstName" :visible="false">First Name</x-smc::input.label>
            <x-smc::input name="filter[firstName]" class="{{ $helper->getClasses('firstName') }}"
                          :value="$helper->getValue('firstName')" placeholder="First..."/>
        </x-smc::input.group>
        <x-smc::input.group>
            <x-smc::input.label for="filter-lastName" :visible="false">First Name</x-smc::input.label>
            <x-smc::input name="filter[lastName]" class="{{ $helper->getClasses('lastName') }}"
                          :value="$helper->getValue('lastName')" placeholder="Last..."/>
        </x-smc::input.group>
        <x-smc::form.filter-buttons></x-smc::form.filter-buttons>
    </x-smc::flex>
</x-smc::form.qs-filter>
