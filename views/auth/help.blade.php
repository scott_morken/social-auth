@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-social-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <h2>Login help</h2>
    <h4>MEID/password help</h4>
    <p>
        Your MEID and password are managed by MCCCD. You can manage your account
        at <a href="https://tools.maricopa.edu" title="Manage your Maricopa account" target="_blank">
            https://tools.maricopa.edu
        </a>
    </p>
    <p>
        If you have logged in with the wrong account, and it won't let you select the correct account
        (meid@maricopa.edu), you can try
        <a href="{{ route('logout.endpoint') }}" title="Logout endpoint">
            logging out the account
        </a>
    </p>
    <h4>Help! It's just not working!</h4>
    <p>
        Occasionally, the browser and the server will get upset with each other and refuse
        to play nicely. The usual result is that the login page keeps reappearing when you login without an
        error message to tell you why it's not working. When this happens, the browser's cache needs to be
        completely cleared.
    </p>
    <h5>Google Chrome</h5>
    <p>
        <a href="https://help.maricopa.edu/TDClient/53/PC/KB/ArticleDet?ID=72"
           target="_blank"
           title="Clear Chrome browser cache knowledge base article"
           >https://help.maricopa.edu/TDClient/53/PC/KB/ArticleDet?ID=72</a>
    </p>
    <h5>Mozilla Firefox</h5>
    <p>
        <a href="https://help.maricopa.edu/TDClient/53/PC/KB/ArticleDet?ID=83"
           target="_blank"
           title="Clear Firefox browser cache knowledge base article"
        >https://help.maricopa.edu/TDClient/53/PC/KB/ArticleDet?ID=72</a>
    </p>
</x-dynamic-component>
