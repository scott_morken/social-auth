@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-social-auth.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <form class="form-signin" method="post" action="{{ route('auth.redirect') }}">
        @csrf
        <button type="submit" name="login" class="btn btn-primary btn-block btn-lg w-100">Click to login to Maricopa Community
            Colleges
        </button>
        <div style="margin-top: .5em;">
            <a href="{{ route('login.help') }}" title="Help logging in" target="_blank">
                Difficulty logging in?
            </a>
            &middot;
            <a href="{{ route('logout.endpoint') }}" title="Logout endpoint">
                Logout endpoint
            </a>
        </div>
        <div style="margin: .5em;">
            Only active students, currently employed staff, and designated 3rd parties are authorized
            to access MCCCD and Phoenix College resources.
        </div>
    </form>
</x-dynamic-component>
