<?php

declare(strict_types=1);

namespace Smorken\SocialAuth;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\View;
use Laravel\Socialite\Contracts\Factory;
use Smorken\Auth\Contracts\Models\User;
use Smorken\Roles\Contracts\Role;
use Smorken\Roles\View\Composers\RoleHandler;
use Smorken\Roles\View\Composers\Roles;
use Smorken\SocialAuth\Admin\Events\ImpersonateUser;
use Smorken\SocialAuth\Admin\Listeners\LogImpersonatedUser;
use Smorken\SocialAuth\Callback\Actions\UpsertEloquentUserAction;
use Smorken\SocialAuth\Callback\Attributes\AttributeResolver;
use Smorken\SocialAuth\Callback\Contracts\Actions\UpsertUserAction;
use Smorken\SocialAuth\Callback\Contracts\ValidateSocialiteUser;
use Smorken\SocialAuth\Callback\Validation\ValidateMeidInUpn;
use Smorken\SocialAuth\Contracts\SocialFactory;
use Smorken\SocialAuth\SocialiteProviders\FakeProvider;
use Smorken\SocialAuth\SocialiteProviders\SessionProvider;
use Smorken\SocialAuth\SocialiteProviders\Support\FakeUserProvider;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        Event::listen(ImpersonateUser::class, LogImpersonatedUser::class);
        $this->loadConfig();
        $this->loadRoutes();
        $this->loadViews();
        $this->handlePublishing();
        $this->bootViewComposers();
        $this->setFakeUserProviderOnFakeProvider();
        $this->bootFakeProviderForSocialite();
        $this->bootSessionProvider();
    }

    public function register(): void
    {
        $this->registerSocialFactory();
        $this->registerActions();
        $this->registerRepositories();
        $this->registerAttributeResolver();
        $this->registerUpsertActionForCallback();
        $this->registerUserValidator();
    }

    /**
     * Extend socialite for 'fake' here because there is no config to load from services
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function bootFakeProviderForSocialite(): void
    {
        if (! $this->app->environment(['local', 'testing'])) {
            return;
        }
        $socialite = $this->app->make(Factory::class);
        $socialite->extend('fake', function () use ($socialite) {
            $config = ['client_id' => 'FAKE', 'client_secret' => 'FAKE', 'redirect' => 'https://fake.local'];

            return $socialite->buildProvider(FakeProvider::class, $config);
        });
    }

    protected function bootSessionProvider(): void
    {
        Auth::provider('sm-social-auth-session', function (Application $app, array $config) {
            $model = $app[User::class];
            $session = $app['session'];

            return new SessionProvider($model, $session);
        });
    }

    protected function bootViewComposers(): void
    {
        if ($this->app->bound(Role::class)) {
            View::composer([
                'sm-social-auth::admin._filter_form',
            ], Roles::class);
            View::composer([
                'sm-social-auth::admin._role',
                'sm-social-auth::admin._role_show',
            ], RoleHandler::class);
        }
    }

    protected function handlePublishing(): void
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('sm-social-auth'),
        ], 'social-auth-config');
        $this->publishes([
            __DIR__.'/../views' => resource_path('views/vendor/sm-social-auth'),
        ], 'social-auth-views');
    }

    protected function loadConfig(): void
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php',
            'sm-social-auth'
        );
    }

    protected function loadRoutes(): void
    {
        if ($this->app['config']->get('sm-social-auth.load_admin_routes', false)) {
            $this->loadRoutesFrom(__DIR__.'/../routes/admin.php');
        }
        if ($this->app['config']->get('sm-social-auth.load_auth_routes', false)) {
            $this->loadRoutesFrom(__DIR__.'/../routes/auth.php');
        }
    }

    protected function loadViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'sm-social-auth');
    }

    protected function registerActions(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-social-auth.actions', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function registerAttributeResolver(): void
    {
        $this->app->scoped(\Smorken\SocialAuth\Callback\Contracts\Attributes\AttributeResolver::class,
            function (Application $app) {
                $cls = $app['config']->get('sm-social-auth.attribute_map_class');
                AttributeResolver::setAttributesClass($cls);

                return new AttributeResolver;
            });
    }

    protected function registerRepositories(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-social-auth.repositories', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function registerSocialFactory(): void
    {
        $this->app->scoped(SocialFactory::class, function (Application $app) {
            $provider = $app['config']->get('sm-social-auth.provider', 'azure-latest');
            if ($provider === 'fake' && ! $app->environment(['local', 'testing'])) {
                throw new \Exception('Fake provider is only available in local or testing environments.');
            }

            return new \Smorken\SocialAuth\SocialFactory(
                $app[Factory::class],
                $provider
            );
        });
    }

    protected function registerUpsertActionForCallback(): void
    {
        $this->app->scoped(UpsertUserAction::class, function (Application $app) {
            $user = $app[\Smorken\Auth\Contracts\Models\User::class];

            return new UpsertEloquentUserAction(
                new AttributeResolver,
                $user
            );
        });
    }

    protected function registerUserValidator(): void
    {
        $this->app->scoped(ValidateSocialiteUser::class, function (Application $app) {
            $cls = $app['config']->get('sm-social-auth.user.validator', ValidateMeidInUpn::class);

            return $app->make($cls);
        });
    }

    protected function setFakeUserProviderOnFakeProvider(): void
    {
        FakeProvider::setFakeUserProvider(new FakeUserProvider([
            'id' => 'abc1234567@maricopa.edu',
            'displayName' => 'Foo Bar',
            'userPrincipalName' => 'abc1234567@maricopa.edu',
            'givenName' => 'Foo',
            'surname' => 'Bar',
            'onPremisesExtensionAttributes' => [
                'extensionAttribute12' => '39999999',
                'extensionAttribute14' => 'foo.bar@example.edu',
            ],
        ]));
    }
}
