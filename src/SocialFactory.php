<?php

declare(strict_types=1);

namespace Smorken\SocialAuth;

use Laravel\Socialite\Contracts\Factory;
use Laravel\Socialite\Contracts\Provider;

class SocialFactory implements \Smorken\SocialAuth\Contracts\SocialFactory
{
    public function __construct(
        protected Factory $factory,
        protected string $providerName
    ) {}

    public function name(): string
    {
        return $this->providerName;
    }

    public function provider(): Provider
    {
        return $this->factory->driver($this->name());
    }
}
