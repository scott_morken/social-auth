<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Validation;

use Laravel\Socialite\Contracts\User;
use Smorken\SocialAuth\Callback\Contracts\ValidateSocialiteUser;

class ValidateMeidInUpn implements ValidateSocialiteUser
{
    protected string $upn;

    public function getMessage(): string
    {
        return "Your selected account does not match 'meid@maricopa.edu'.  You provided '$this->upn'.";
    }

    public function validate(User $user): bool
    {
        $this->upn = $user->getEmail();

        return (bool) preg_match('/[A-z]{3,5}[0-9]{5,7}/', (string) $user->getEmail());
    }
}
