<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\ValueObjects;

use Smorken\Auth\Contracts\Models\User;

class SessionStoreVO
{
    public function __construct(
        public string $externalId,
        public User $user,
        public mixed $id,
        public mixed $username,
    ) {}
}
