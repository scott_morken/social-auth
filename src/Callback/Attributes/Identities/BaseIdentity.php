<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Attributes\Identities;

use Laravel\Socialite\Contracts\User;
use Smorken\SocialAuth\Callback\Contracts\Attributes\Identity;

abstract class BaseIdentity implements Identity
{
    public function __construct(protected User $user) {}
}
