<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Attributes\Identities;

use Smorken\SocialAuth\Callback\Contracts\Attributes\Identity;

class AzureMeidIdentity extends BaseIdentity implements Identity
{
    public function toArray(): array
    {
        $id = $this->user->getEmail();
        $meid = explode('@', strtolower((string) $id))[0];

        return ['username' => $meid];
    }
}
