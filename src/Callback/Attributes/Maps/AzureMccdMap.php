<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Attributes\Maps;

use Laravel\Socialite\Contracts\User;
use Smorken\SocialAuth\Callback\Attributes\Identities\AzureMeidIdentity;
use Smorken\SocialAuth\Callback\Contracts\Attributes\AttributeMap;
use Smorken\SocialAuth\Callback\Contracts\Attributes\Identity;

class AzureMccdMap extends BaseAttributeMap implements AttributeMap
{
    protected function createAttributes(User $user): array
    {
        return [
            'id' => $this->getId($user),
            'first_name' => $this->getFirstName($user),
            'last_name' => $this->getLastName($user),
            'email' => $this->getEmail($user),
        ];
    }

    protected function createIdentity(User $user): Identity
    {
        return new AzureMeidIdentity($user);
    }

    protected function fromRaw(User $user, string $key, mixed $default = ''): mixed
    {
        // @phpstan-ignore method.notFound
        $v = $user->getRaw()[$key] ?? null;

        return $v ?: $default;
    }

    protected function getEmail(User $user): string
    {
        return $this->getExtensionAttribute($user, 'extensionAttribute14', $user->getEmail());
    }

    protected function getExtensionAttribute(User $user, string $key, mixed $default = ''): string
    {
        $v = $this->getExtensionAttributes($user)[$key] ?? null;

        return $v ?: $default;
    }

    protected function getExtensionAttributes(User $user): array
    {
        return $this->fromRaw($user, 'onPremisesExtensionAttributes', []);
    }

    protected function getFirstName(User $user): string
    {
        return $this->fromRaw($user, 'givenName', '');
    }

    protected function getId(User $user): string
    {
        return $this->getExtensionAttribute($user, 'extensionAttribute12', explode('@', (string) $user->getEmail())[0].'-ERROR');
    }

    protected function getLastName(User $user): string
    {
        return $this->fromRaw($user, 'surname');
    }
}
