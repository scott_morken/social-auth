<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Attributes\Maps;

use Laravel\Socialite\Contracts\User;
use Smorken\SocialAuth\Callback\Contracts\Attributes\AttributeMap;
use Smorken\SocialAuth\Callback\Contracts\Attributes\Identity;

abstract class BaseAttributeMap implements AttributeMap
{
    protected array $attributes;

    protected Identity $identity;

    public function __construct(protected User $user)
    {
        $this->init();
    }

    public function identity(): Identity
    {
        return $this->identity;
    }

    public function attributes(): array
    {
        return $this->attributes;
    }

    protected function init(): void
    {
        $this->identity = $this->createIdentity($this->user);
        $this->attributes = $this->createAttributes($this->user);
    }

    abstract protected function createIdentity(User $user): Identity;

    abstract protected function createAttributes(User $user): array;

    public function attributesForCreate(): array
    {
        return [
            ...$this->identity()->toArray(),
            ...$this->attributes(),
        ];
    }

    public function attributesForUpdate(): array
    {
        return $this->attributes();
    }
}
