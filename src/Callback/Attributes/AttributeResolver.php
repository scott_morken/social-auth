<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Attributes;

use Laravel\Socialite\Contracts\User as SocialiteUser;
use Smorken\SocialAuth\Callback\Contracts\Attributes\AttributeMap;

class AttributeResolver implements \Smorken\SocialAuth\Callback\Contracts\Attributes\AttributeResolver
{
    protected static string $attributesClass;

    public static function setAttributesClass(string $attributesClass): void
    {
        self::$attributesClass = $attributesClass;
    }

    public function resolve(SocialiteUser $user): AttributeMap
    {
        return new self::$attributesClass($user);
    }
}
