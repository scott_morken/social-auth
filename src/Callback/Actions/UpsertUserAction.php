<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Actions;

use Laravel\Socialite\Contracts\User as SocialiteUser;
use Smorken\Auth\Contracts\Models\User;
use Smorken\Domain\Actions\Action;
use Smorken\SocialAuth\Callback\Contracts\Attributes\AttributeMap;
use Smorken\SocialAuth\Callback\Contracts\Attributes\AttributeResolver;
use Smorken\SocialAuth\Callback\Contracts\Attributes\Identity;

/**
 * @template TUser of User
 */
abstract class UpsertUserAction extends Action implements \Smorken\SocialAuth\Callback\Contracts\Actions\UpsertUserAction
{
    public function __construct(protected AttributeResolver $attributeResolver)
    {
        parent::__construct();
    }

    /**
     * @return TUser
     */
    abstract protected function createUser(SocialiteUser $socialiteUser, array $attributes): User;

    /**
     * @return TUser|null
     */
    abstract protected function findUser(SocialiteUser $socialiteUser, Identity $identity): ?User;

    /**
     * @param  TUser  $user
     */
    abstract protected function updateUser(User $user, array $attributes, SocialiteUser $socialiteUser): void;

    /**
     * @return TUser
     */
    public function __invoke(SocialiteUser $user): User
    {
        $attributes = $this->getAttributesForSocialiteUser($user);

        return $this->findOrCreateUser($user, $attributes);
    }

    /**
     * @return TUser
     */
    protected function findOrCreateUser(SocialiteUser $socialiteUser, AttributeMap $attributes): User
    {
        $user = $this->findUser($socialiteUser, $attributes->identity());
        if (! $user) {
            return $this->createUser($socialiteUser, $attributes->attributesForCreate());
        }
        if ($this->shouldUpdateUser($user, $attributes->attributesForUpdate())) {
            $this->updateUser($user, $attributes->attributesForUpdate(), $socialiteUser);
        }

        return $user;
    }

    protected function getAttributesForSocialiteUser(SocialiteUser $user): AttributeMap
    {
        return $this->attributeResolver->resolve($user);
    }

    /**
     * @param  TUser  $user
     */
    protected function shouldUpdateUser(User $user, array $attributes): bool
    {
        foreach ($attributes as $key => $value) {
            if ($value && $value !== $user->$key) {
                return true;
            }
        }

        return false;
    }
}
