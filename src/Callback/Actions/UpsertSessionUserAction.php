<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Actions;

use Illuminate\Session\SessionManager;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use Smorken\Auth\Models\VO\User;
use Smorken\SocialAuth\Callback\Contracts\Attributes\AttributeResolver;
use Smorken\SocialAuth\Callback\Contracts\Attributes\Identity;
use Smorken\SocialAuth\Callback\Support\SessionKey;
use Smorken\SocialAuth\Callback\ValueObjects\SessionStoreVO;

/**
 * @extends \Smorken\SocialAuth\Callback\Actions\UpsertUserAction<\Smorken\Auth\Models\VO\User>
 */
class UpsertSessionUserAction extends UpsertUserAction implements \Smorken\SocialAuth\Callback\Contracts\Actions\UpsertUserAction
{
    public function __construct(AttributeResolver $attributeResolver, protected SessionManager $session)
    {
        parent::__construct($attributeResolver);
    }

    protected function createUser(SocialiteUser $socialiteUser, array $attributes): \Smorken\Auth\Contracts\Models\User
    {
        $user = new User($attributes);
        if ($user->id) {
            $vo = new SessionStoreVO(
                $socialiteUser->getId(),
                $user,
                $user->id,
                $user->username
            );
            $this->session->put(SessionKey::get(), $vo);
        }

        return $user;
    }

    protected function findUser(SocialiteUser $socialiteUser, Identity $identity): ?\Smorken\Auth\Contracts\Models\User
    {
        $sessionVO = $this->session->get(SessionKey::get());
        if ($sessionVO && $sessionVO->externalId === $socialiteUser->getId()) {
            return $sessionVO->user;
        }

        return null;
    }

    protected function updateUser(
        \Smorken\Auth\Contracts\Models\User $user,
        array $attributes,
        SocialiteUser $socialiteUser
    ): void {
        $user->fill($attributes);
        $vo = new SessionStoreVO(
            $socialiteUser->getId(),
            $user,
            $user->id,
            $user->username
        );
        $this->session->put(SessionKey::get(), $vo);
    }
}
