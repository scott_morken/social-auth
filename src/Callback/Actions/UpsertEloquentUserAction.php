<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Actions;

use Laravel\Socialite\Contracts\User as SocialiteUser;
use Smorken\Auth\Contracts\Models\User;
use Smorken\SocialAuth\Callback\Contracts\Attributes\AttributeResolver;
use Smorken\SocialAuth\Callback\Contracts\Attributes\Identity;

/**
 * @template TUser of \Smorken\Auth\Models\Eloquent\User
 *
 * @extends \Smorken\SocialAuth\Callback\Actions\UpsertUserAction<\Smorken\Auth\Models\Eloquent\User>
 */
class UpsertEloquentUserAction extends UpsertUserAction implements \Smorken\SocialAuth\Callback\Contracts\Actions\UpsertUserAction
{
    /**
     * @param  TUser  $user
     */
    public function __construct(AttributeResolver $attributeResolver, protected User $user)
    {
        parent::__construct($attributeResolver);
    }

    protected function createUser(SocialiteUser $socialiteUser, array $attributes): User
    {
        $user = $this->user->newInstance();
        $user->fill($attributes);
        $user->save();

        return $user;
    }

    protected function findUser(SocialiteUser $socialiteUser, Identity $identity): ?User
    {
        $query = $this->user->newQuery();
        foreach ($identity->toArray() as $key => $value) {
            $query->where($key, '=', $value);
        }

        /** @var \Smorken\Auth\Models\Eloquent\User|null */
        return $query->first();
    }

    protected function updateUser(User $user, array $attributes, SocialiteUser $socialiteUser): void
    {
        $user->fill($attributes);

        $user->save();
    }
}
