<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Support;

class SessionKey
{
    public static function get(): string
    {
        return 'sm_social_auth_user';
    }
}
