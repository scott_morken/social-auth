<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Contracts\Actions;

use Laravel\Socialite\Contracts\User as SocialiteUser;
use Smorken\Auth\Contracts\Models\User;
use Smorken\Domain\Actions\Contracts\Action;

interface UpsertUserAction extends Action
{
    public function __invoke(SocialiteUser $user): User;
}
