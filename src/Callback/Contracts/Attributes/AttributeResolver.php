<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Contracts\Attributes;

use Laravel\Socialite\Contracts\User as SocialiteUser;

interface AttributeResolver
{
    public function resolve(SocialiteUser $user): AttributeMap;

    public static function setAttributesClass(string $attributesClass): void;
}
