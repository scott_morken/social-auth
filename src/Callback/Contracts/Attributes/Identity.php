<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Contracts\Attributes;

interface Identity
{
    public function toArray(): array;
}
