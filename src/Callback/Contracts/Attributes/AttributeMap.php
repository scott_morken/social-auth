<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Contracts\Attributes;

interface AttributeMap
{
    public function attributes(): array;

    public function attributesForCreate(): array;

    public function attributesForUpdate(): array;

    public function identity(): Identity;
}
