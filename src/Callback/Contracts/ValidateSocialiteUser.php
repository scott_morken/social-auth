<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Callback\Contracts;

use Laravel\Socialite\Contracts\User;

interface ValidateSocialiteUser
{
    public function getMessage(): string;

    public function validate(User $user): bool;
}
