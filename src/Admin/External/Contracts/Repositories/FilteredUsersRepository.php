<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\External\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\FilteredRepository;

interface FilteredUsersRepository extends FilteredRepository {}
