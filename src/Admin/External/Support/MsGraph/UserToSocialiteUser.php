<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\External\Support\MsGraph;

use Laravel\Socialite\Contracts\User as SocialiteUser;
use Microsoft\Graph\Beta\Generated\Models\User;

class UserToSocialiteUser
{
    public static function convert(User $user): SocialiteUser
    {
        return (new \SocialiteProviders\Manager\OAuth2\User)->setRaw(self::toArray($user))->map([
            'id' => $user->getId(),
            'nickname' => null,
            'name' => $user->getDisplayName(),
            'email' => $user->getUserPrincipalName(),
            'avatar' => null,
        ]);
    }

    public static function toArray(User $user): array
    {
        $toArray = function (array $parts) use (&$toArray) {
            $result = [];
            foreach ($parts as $k => $v) {
                if (is_object($v) && method_exists($v, 'getBackingStore')) {
                    $result[$k] = $toArray($v->getBackingStore()->enumerate());
                } else {
                    $result[$k] = $v;
                }
            }

            return $result;
        };

        return $toArray($user->getBackingStore()->enumerate());
    }
}
