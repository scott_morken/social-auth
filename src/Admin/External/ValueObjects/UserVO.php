<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\External\ValueObjects;

use Laravel\Socialite\Contracts\User;
use Smorken\SocialAuth\Callback\Attributes\AttributeResolver;

class UserVO
{
    public function __construct(
        public string $externalId,
        public int|string $id,
        public string $username,
        public string $first_name,
        public string $last_name,
        public string $email
    ) {}

    public static function fromSocialiteUser(User $user): self
    {
        $resolver = new AttributeResolver;
        $map = $resolver->resolve($user);
        $attributes = ['externalId' => $user->getId(), ...$map->attributesForCreate()];

        return new self(
            ...$attributes
        );
    }
}
