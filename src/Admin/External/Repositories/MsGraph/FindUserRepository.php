<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\External\Repositories\MsGraph;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use Microsoft\Graph\Beta\Generated\Models\User;
use Smorken\Domain\Cache\Constants\CacheType;
use Smorken\Domain\Repositories\RetrieveRepository;
use Smorken\MsGraph\Users\Users;
use Smorken\SocialAuth\Admin\External\Support\MsGraph\UserToSocialiteUser;

class FindUserRepository extends RetrieveRepository implements \Smorken\SocialAuth\Admin\External\Contracts\Repositories\FindUserRepository
{
    protected CacheType $cacheType = CacheType::INSTANCE;

    public function __construct(protected Users $model) {}

    protected function convertToSocialiteUser(User $user): SocialiteUser
    {
        return UserToSocialiteUser::convert($user);
    }

    protected function retrieve(mixed $id, bool $throw): ?SocialiteUser
    {
        $user = $this->model->newQuery()
            ->find($id);
        if (! $user && $throw) {
            throw new ModelNotFoundException;
        }

        return $user ? $this->convertToSocialiteUser($user) : null;
    }
}
