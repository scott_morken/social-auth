<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\External\Repositories\MsGraph;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Microsoft\Graph\Beta\Generated\Models\ODataErrors\ODataError;
use Microsoft\Graph\Beta\Generated\Models\User;
use Microsoft\Graph\Beta\Generated\Models\UserCollectionResponse;
use Smorken\Domain\Repositories\FilteredRepository;
use Smorken\MsGraph\Users\Users;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\SocialAuth\Admin\External\Support\MsGraph\UserToSocialiteUser;
use Smorken\SocialAuth\Admin\External\ValueObjects\UserVO;
use Smorken\Support\Contracts\Filter;

/**
 * @extends FilteredRepository<Users>
 */
class FilteredUsersRepository extends FilteredRepository implements \Smorken\SocialAuth\Admin\External\Contracts\Repositories\FilteredUsersRepository
{
    public function __construct(
        protected Users $model,
        protected ExceptionHandler $exceptionHandler
    ) {}

    protected function convertToSocialiteUser(User $user): \Laravel\Socialite\Contracts\User
    {
        return UserToSocialiteUser::convert($user);
    }

    protected function convertToUserVO(\Laravel\Socialite\Contracts\User $user): UserVO
    {
        return UserVO::fromSocialiteUser($user);
    }

    protected function getFiltered(QueryStringFilter|Filter $filter, int $perPage): iterable|Paginator|Collection
    {
        $f = $this->getQueryFilter($filter);
        if (! $f) {
            return new Collection;
        }
        $response = $this->getGraphUsers($f, $perPage);

        return $this->responseToUserCollection($response);
    }

    protected function getGraphUsers(\Smorken\MsGraph\Contracts\Filter $filter, int $perPage): UserCollectionResponse
    {
        try {
            return $this->model->newQuery()
                ->filter($filter)
                ->select([
                    'id',
                    'displayName',
                    'givenName',
                    'surname',
                    'userPrincipalName',
                    'mail',
                    'onPremisesExtensionAttributes',
                ])
                ->top($perPage)
                ->get();
        } catch (IdentityProviderException $e) {
            $this->exceptionHandler->report(new \Exception($e->getMessage().' - '.$this->responseBodyToString($e->getResponseBody())));
            throw new AuthorizationException('Cannot retrieve external users - unauthorized');
        } catch (ODataError $e) {
            $message = $e->getPrimaryErrorMessage();
            $this->exceptionHandler->report(new \Exception($e->getMessage().' - '.$message));
            if (str_starts_with($message, 'Insufficient privileges')) {
                throw new AuthorizationException('Cannot retrieve external users - unauthorized');
            }
            throw $e;
        }
    }

    protected function getQueryFilter(QueryStringFilter $filter): ?\Smorken\MsGraph\Contracts\Filter
    {
        $f = new \Smorken\MsGraph\Query\Filter;
        $meid = $filter->filter('username')->get();
        if ($meid) {
            $f->startsWith('userPrincipalName', $meid);

            return $f;
        }
        $firstName = $filter->filter('firstName')->get();
        $lastName = $filter->filter('lastName')->get();
        if ($firstName) {
            $f->startsWith('givenName', $firstName);
        }
        if ($lastName) {
            $f->startsWith('surname', $lastName);
        }

        return $firstName || $lastName ? $f : null;
    }

    protected function responseBodyToString(mixed $response): string
    {
        if (is_array($response)) {
            return json_encode($response);
        }

        return $response;
    }

    protected function responseToUserCollection(UserCollectionResponse $response): Collection
    {
        $users = new Collection;
        foreach ($response->getValue() as $user) {
            $users->push($this->convertToUserVO($this->convertToSocialiteUser($user)));
        }

        return $users;
    }
}
