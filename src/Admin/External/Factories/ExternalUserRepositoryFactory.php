<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\External\Factories;

use Illuminate\Support\Collection;
use Laravel\Socialite\Contracts\User;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\SocialAuth\Admin\External\Contracts\Repositories\FilteredUsersRepository;
use Smorken\SocialAuth\Admin\External\Contracts\Repositories\FindUserRepository;

class ExternalUserRepositoryFactory extends RepositoryFactory
{
    protected array $handlers = [
        'find' => FindUserRepository::class,
        'filtered' => FilteredUsersRepository::class,
    ];

    public function filtered(
        QueryStringFilter $filter,
        int $perPage = 25
    ): Collection {
        return $this->handlerForFiltered($filter, $perPage);
    }

    public function find(string $id, bool $throw = true): ?User
    {
        return $this->handlerForFind($id, $throw);
    }
}
