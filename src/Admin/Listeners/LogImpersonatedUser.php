<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Listeners;

use Psr\Log\LoggerInterface;
use Smorken\SocialAuth\Admin\Events\ImpersonateUser;

class LogImpersonatedUser
{
    public function __construct(protected LoggerInterface $logger) {}

    public function handle(ImpersonateUser $impersonateUser): void
    {
        $this->logger->warning('Impersonating user', $impersonateUser->toArray());
    }
}
