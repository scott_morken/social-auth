<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Validation\RuleProviders;

class UserRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'id' => 'required|int',
            'username' => [
                'required',
                'max:255',
            ],
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            ...$overrides,
        ];
    }
}
