<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Actions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;
use Smorken\Auth\Contracts\Models\User;
use Smorken\Domain\Actions\Contracts\Upsertable as UpsertableContract;
use Smorken\Domain\Actions\EloquentUpsertAction;
use Smorken\Domain\Actions\Upsertable;
use Smorken\Model\Contracts\Model as ModelContract;
use Smorken\Roles\Contracts\Actions\UpsertRoleUserAction;
use Smorken\SocialAuth\Admin\Validation\RuleProviders\UserRules;

/**
 * @extends EloquentUpsertAction<\Smorken\Auth\Models\Eloquent\User, Upsertable>
 */
class UpsertUserAction extends EloquentUpsertAction implements \Smorken\SocialAuth\Admin\Contracts\Actions\UpsertUserAction
{
    protected string $rulesProvider = UserRules::class;

    public function __construct(
        User $model,
        protected ?UpsertRoleUserAction $upsertRoleUserAction = null
    ) {
        parent::__construct($model);
    }

    protected function forgetKeys(array $items, array $keys): array
    {
        foreach ($keys as $key) {
            unset($items[$key]);
        }

        return $items;
    }

    protected function getRulesForCreate(): array
    {
        $rules = parent::getRulesForCreate();
        $rules['username'][] = Rule::unique('users');

        return $rules;
    }

    protected function getRulesForUpdate(Model|ModelContract $model): array
    {
        $rules = parent::getRulesForUpdate($model);
        $rules['username'][] = Rule::unique('users')->ignore($model);

        return $this->forgetKeys($rules, ['id']);
    }

    protected function getUpdateAttributes(UpsertableContract $upsertable): array
    {
        return $this->forgetKeys(parent::getUpdateAttributes($upsertable), ['id']);
    }

    protected function postExecuteFromUpsertable(
        Model|ModelContract $model,
        UpsertableContract $upsertable
    ): Model|ModelContract {
        $roleId = $upsertable->getAttributes()['role'] ?? null;
        if ($roleId && $this->upsertRoleUserAction) {
            $upsertable = new Upsertable(['role_id' => (int) $roleId], ['user_id' => (int) $model->getKey()]);
            ($this->upsertRoleUserAction)($upsertable);
        }

        return $model;
    }
}
