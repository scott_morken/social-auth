<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Actions\Results;

use Smorken\Auth\Contracts\Models\User;

class ImpersonateResult implements \Smorken\SocialAuth\Admin\Contracts\Actions\Results\ImpersonateResult
{
    public function __construct(
        public User $user,
        public int $id,
        public int $originalId,
        public string $redirectTo = '/'
    ) {}
}
