<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Actions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Event;
use Smorken\Domain\Actions\Action;
use Smorken\SocialAuth\Admin\Contracts\Actions\Results\ImpersonateResult;
use Smorken\SocialAuth\Admin\Contracts\Repositories\FindUserRepository;
use Smorken\SocialAuth\Admin\Events\ImpersonateUser;

class ImpersonateUserAction extends Action implements \Smorken\SocialAuth\Admin\Contracts\Actions\ImpersonateUserAction
{
    public function __construct(
        protected Guard $guard,
        protected FindUserRepository $findUserRepository
    ) {
        parent::__construct();
    }

    public function __invoke(int $userId): ?ImpersonateResult
    {
        $this->checkAuthorized();
        $originalId = (int) $this->guard->id();
        if ($userId === $originalId) {
            return null;
        }
        $user = ($this->findUserRepository)($userId);
        $this->guard->loginUsingId($user->getAuthIdentifier());
        Event::dispatch(new ImpersonateUser($originalId, $userId));

        return new \Smorken\SocialAuth\Admin\Actions\Results\ImpersonateResult($user, $userId, $originalId);
    }

    protected function checkAuthorized(): void
    {
        if (! $this->guard->user()->can('role-admin')) {
            throw new AuthorizationException;
        }
    }
}
