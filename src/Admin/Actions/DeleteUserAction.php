<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Actions;

use Illuminate\Database\Eloquent\Model;
use Smorken\Auth\Contracts\Models\User;
use Smorken\Domain\Actions\EloquentDeleteAction;

/**
 * @extends EloquentDeleteAction<\Smorken\Auth\Models\Eloquent\User>
 */
class DeleteUserAction extends EloquentDeleteAction implements \Smorken\SocialAuth\Admin\Contracts\Actions\DeleteUserAction
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    protected function preDeleteModelActions(Model|\Smorken\Model\Contracts\Model $model): void
    {
        if (method_exists($model, 'roleUser') && is_int($model->id)) {
            $model->roleUser()->delete();
        }
    }
}
