<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Contracts\Repositories;

interface FindUserRepository extends \Smorken\Auth\Contracts\Repositories\FindUserRepository {}
