<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Contracts\Repositories;

interface FilteredUsersRepository extends \Smorken\Auth\Contracts\Repositories\FilteredUsersRepository {}
