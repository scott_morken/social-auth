<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\Action;
use Smorken\SocialAuth\Admin\Contracts\Actions\Results\ImpersonateResult;

interface ImpersonateUserAction extends Action
{
    public function __invoke(int $userId): ?ImpersonateResult;
}
