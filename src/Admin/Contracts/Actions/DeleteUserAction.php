<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\DeleteAction;

interface DeleteUserAction extends DeleteAction {}
