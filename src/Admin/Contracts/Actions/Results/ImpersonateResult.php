<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Contracts\Actions\Results;

use Smorken\Domain\Actions\Contracts\Results\Result;

/**
 * @property \Smorken\Auth\Contracts\Models\User $user
 * @property int $id
 * @property int $originalId
 * @property string $redirectTo
 *
 * @phpstan-require-extends \Smorken\SocialAuth\Admin\Actions\Results\ImpersonateResult
 */
interface ImpersonateResult extends Result {}
