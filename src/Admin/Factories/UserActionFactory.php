<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Factories;

use Smorken\Domain\Factories\ActionFactory;
use Smorken\SocialAuth\Admin\Contracts\Actions\DeleteUserAction;
use Smorken\SocialAuth\Admin\Contracts\Actions\UpsertUserAction;

class UserActionFactory extends ActionFactory
{
    protected array $handlers = [
        'delete' => DeleteUserAction::class,
        'upsert' => UpsertUserAction::class,
    ];
}
