<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Factories;

use Smorken\Auth\Contracts\Models\User;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\SocialAuth\Admin\Contracts\Repositories\FilteredUsersRepository;
use Smorken\SocialAuth\Admin\Contracts\Repositories\FindUserRepository;
use Smorken\Support\Contracts\Filter;

class UserRepositoryFactory extends RepositoryFactory
{
    protected array $handlers = [
        'find' => FindUserRepository::class,
        'filtered' => FilteredUsersRepository::class,
    ];

    public function emptyModel(): mixed
    {
        return $this->handlerForEmptyModel();
    }

    public function filtered(
        QueryStringFilter|Filter $filter,
        int $perPage = 20
    ): \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Support\Collection|iterable {
        return $this->handlerForFiltered($filter, $perPage);
    }

    public function find(mixed $id, bool $throw = true): ?User
    {
        return $this->handlerForFind($id, $throw);
    }
}
