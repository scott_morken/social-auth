<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Events;

use Illuminate\Contracts\Support\Arrayable;

class ImpersonateUser implements Arrayable
{
    public function __construct(
        public string|int $originalUserId,
        public string|int $impersonatedUserId
    ) {}

    public function toArray(): array
    {
        return [
            'original' => $this->originalUserId,
            'impersonated' => $this->impersonatedUserId,
        ];
    }
}
