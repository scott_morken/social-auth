<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Repositories;

class FilteredUsersRepository extends \Smorken\Auth\Repositories\FilteredUsersRepository implements \Smorken\SocialAuth\Admin\Contracts\Repositories\FilteredUsersRepository {}
