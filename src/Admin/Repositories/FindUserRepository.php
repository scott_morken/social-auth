<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Admin\Repositories;

class FindUserRepository extends \Smorken\Auth\Repositories\FindUserRepository implements \Smorken\SocialAuth\Admin\Contracts\Repositories\FindUserRepository {}
