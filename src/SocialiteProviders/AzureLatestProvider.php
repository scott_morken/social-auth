<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\SocialiteProviders;

use SocialiteProviders\Azure\Provider;

class AzureLatestProvider extends Provider
{
    protected $graphUrl = 'https://graph.microsoft.com/beta/me';
}
