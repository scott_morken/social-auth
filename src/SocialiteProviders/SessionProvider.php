<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\SocialiteProviders;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Session\SessionManager;
use Smorken\Auth\Contracts\Models\User;
use Smorken\SocialAuth\Callback\Support\SessionKey;
use Smorken\SocialAuth\Callback\ValueObjects\SessionStoreVO;

class SessionProvider implements UserProvider
{
    public function __construct(protected User $user, protected SessionManager $session) {}

    public function rehashPasswordIfRequired(Authenticatable $user, array $credentials, bool $force = false)
    {
        // do nothing
    }

    public function retrieveByCredentials(array $credentials)
    {
        return null;
    }

    public function retrieveById($identifier)
    {
        $sessionVO = $this->getSessionVO();
        if ($sessionVO && $sessionVO->id === $identifier) {
            return $sessionVO->user;
        }

        return null;
    }

    public function retrieveByToken($identifier, $token)
    {
        return null;
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        // do nothing
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return false;
    }

    protected function getSessionVO(): ?SessionStoreVO
    {
        return $this->session->get(SessionKey::get());
    }
}
