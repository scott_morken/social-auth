<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\SocialiteProviders;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Smorken\SocialAuth\SocialiteProviders\Support\FakeUserProvider;
use SocialiteProviders\Manager\OAuth2\AbstractProvider;
use SocialiteProviders\Manager\OAuth2\User;

class FakeProvider extends AbstractProvider
{
    protected static ?FakeUserProvider $userProvider = null;

    public static function setFakeUserProvider(FakeUserProvider $userProvider, bool $default = true): void
    {
        if (! self::$userProvider || $default === false) {
            self::$userProvider = $userProvider;
        }
    }

    public function getAccessTokenResponse($code): array
    {
        return ['access_token' => 'FAKETOKEN'];
    }

    public function redirect(): RedirectResponse
    {
        return Redirect::route('auth.callback');
    }

    protected function getAuthUrl($state): string
    {
        return 'https://fake.local/auth';
    }

    protected function getTokenUrl(): string
    {
        return 'https://fake.local/token';
    }

    protected function getUserByToken($token): array
    {
        return self::$userProvider->getUser();
    }

    protected function mapUserToObject(array $user): \Laravel\Socialite\Contracts\User
    {
        return (new User)->setRaw($user)->map([
            'id' => $user['id'],
            'nickname' => null,
            'name' => $user['displayName'],
            'email' => $user['userPrincipalName'],
            'avatar' => null,
        ]);
    }
}
