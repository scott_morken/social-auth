<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\SocialiteProviders\Support;

class FakeUserProvider
{
    public function __construct(protected array $user) {}

    public function getUser(): array
    {
        return $this->user;
    }
}
