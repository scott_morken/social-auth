<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\SocialiteProviders;

use SocialiteProviders\Manager\SocialiteWasCalled;

class AzureLatestExtendSocialite
{
    public function __invoke(SocialiteWasCalled $socialiteWasCalled): void
    {
        $socialiteWasCalled->extendSocialite('azure-latest', AzureLatestProvider::class);
    }
}
