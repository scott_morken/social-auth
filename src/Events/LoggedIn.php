<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Events;

use Illuminate\Foundation\Events\Dispatchable;

class LoggedIn
{
    use Dispatchable;

    public function __construct(public readonly string $username) {}
}
