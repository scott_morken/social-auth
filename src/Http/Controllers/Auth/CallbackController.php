<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Http\Controllers\Auth;

use Illuminate\Contracts\Auth\Factory;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use Psr\Http\Client\RequestExceptionInterface;
use Smorken\Auth\Contracts\Models\User;
use Smorken\Controller\Controller;
use Smorken\SocialAuth\Callback\Contracts\Actions\UpsertUserAction;
use Smorken\SocialAuth\Callback\Contracts\ValidateSocialiteUser;
use Smorken\SocialAuth\Contracts\SocialFactory;
use Smorken\SocialAuth\Events\LoggedIn;

class CallbackController extends Controller
{
    protected MessageBag $messages;

    protected string $redirectTo = '/';

    public function __construct(
        protected SocialFactory $factory,
        protected Factory $authFactory,
        protected UpsertUserAction $upsertUserAction,
        protected ValidateSocialiteUser $validateUser,
        protected ExceptionHandler $exceptionHandler
    ) {
        parent::__construct();
        $this->messages = new \Illuminate\Support\MessageBag;
    }

    public function __invoke(Request $request): JsonResponse|RedirectResponse
    {
        $socialiteUser = $this->getSocialiteUser($request);
        if (! $socialiteUser) {
            return $this->sendFailedLoginResponse($request);
        }
        if (! $this->validateSocialiteUser($socialiteUser)) {
            return $this->sendFailedLoginResponse($request);
        }
        $user = $this->createUserFromSocialiteUser($socialiteUser);
        // @phpstan-ignore property.notFound
        LoggedIn::dispatch($user->username);
        $this->authFactory->guard()->login($user);

        return $this->sendLoginResponse($request);
    }

    public function redirectPath(): string
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }

    protected function createUserFromSocialiteUser(SocialiteUser $user): User
    {
        return ($this->upsertUserAction)($user);
    }

    protected function flashMessages(Request $request): void
    {
        if ($request->wantsJson()) {
            return;
        }
        foreach ($this->messages->toArray() as $key => $messages) {
            foreach ($messages as $i => $message) {
                $request->session()->flash($key.':'.$i, $message);
            }
        }
    }

    protected function getSocialiteUser(Request $request): ?SocialiteUser
    {
        try {
            // @phpstan-ignore method.notFound
            return $this->factory->provider()->stateless()->user();
        } catch (RequestExceptionInterface $e) {
            $this->exceptionHandler->report($e);
            $this->messages->add('flash:danger',
                'There was an error connecting to the authentication provider. Please try again in a few minutes.');
        }

        return null;
    }

    protected function sendFailedLoginResponse(Request $request): JsonResponse|RedirectResponse
    {
        $this->flashMessages($request);

        return $request->wantsJson()
            ? new JsonResponse(['errors' => $this->messages->toArray()], 401)
            : Redirect::route('login');
    }

    protected function sendLoginResponse(Request $request): JsonResponse|RedirectResponse
    {
        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : Redirect::intended($this->redirectPath());
    }

    protected function validateSocialiteUser(SocialiteUser $user): bool
    {
        if (! $this->validateUser->validate($user)) {
            $this->messages->add('flash:danger', $this->validateUser->getMessage());

            return false;
        }

        return true;
    }
}
