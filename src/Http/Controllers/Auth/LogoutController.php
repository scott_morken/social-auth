<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Http\Controllers\Auth;

use Illuminate\Contracts\Auth\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Smorken\Controller\Controller;
use Smorken\SocialAuth\Contracts\SocialFactory;

class LogoutController extends Controller
{
    public function __construct(
        protected Factory $authFactory,
        protected SocialFactory $factory
    ) {
        parent::__construct();
    }

    public function __invoke(Request $request): JsonResponse|RedirectResponse
    {
        $this->clearSession($request);

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : Redirect::to('/');
    }

    public function endpoint(Request $request): RedirectResponse
    {
        $this->clearSession($request);
        // @phpstan-ignore method.notFound
        $azureLogoutUrl = $this->factory->provider()->getLogoutUrl(route('login'));

        return Redirect::to($azureLogoutUrl);
    }

    protected function clearSession(Request $request): void
    {
        $this->authFactory->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
    }
}
