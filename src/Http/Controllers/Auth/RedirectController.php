<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Http\Controllers\Auth;

use Illuminate\Http\RedirectResponse;
use Smorken\Controller\Controller;
use Smorken\SocialAuth\Contracts\SocialFactory;

class RedirectController extends Controller
{
    public function __construct(protected SocialFactory $factory)
    {
        parent::__construct();
    }

    public function __invoke(): RedirectResponse
    {
        // @phpstan-ignore method.notFound
        return $this->factory->provider()->stateless()->redirect();
    }
}
