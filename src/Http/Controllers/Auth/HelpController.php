<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Http\Controllers\Auth;

use Illuminate\Contracts\View\View;
use Smorken\Controller\View\Controller;

class HelpController extends Controller
{
    protected string $baseView = 'sm-social-auth::auth';

    public function __invoke(): View
    {
        return $this->makeView($this->getViewName('help'));
    }
}
