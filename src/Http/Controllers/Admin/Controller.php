<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Http\Controllers\Admin;

use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Smorken\Controller\Contracts\View\WithResource\WithActionFactory;
use Smorken\Controller\Contracts\View\WithResource\WithRepositoryFactory;
use Smorken\Controller\View\WithResource\CrudController;
use Smorken\Domain\Actions\Contracts\Upsertable;
use Smorken\Domain\ViewModels\FilteredViewModel;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\SocialAuth\Admin\Contracts\Actions\ImpersonateUserAction;
use Smorken\SocialAuth\Admin\External\Factories\ExternalUserRepositoryFactory;
use Smorken\SocialAuth\Admin\Factories\UserActionFactory;
use Smorken\SocialAuth\Admin\Factories\UserRepositoryFactory;
use Smorken\SocialAuth\Callback\Contracts\Actions\UpsertUserAction;

class Controller extends CrudController implements WithActionFactory, WithRepositoryFactory
{
    protected string $baseView = 'sm-social-auth::admin';

    public function __construct(
        UserActionFactory $actionFactory,
        UserRepositoryFactory $repositoryFactory,
        protected ImpersonateUserAction $impersonateUserAction,
        protected ExternalUserRepositoryFactory $externalUserRepositoryFactory,
        protected UpsertUserAction $upsertSocialiteUserAction
    ) {
        parent::__construct();
        $this->actionFactory = $actionFactory;
        $this->repositoryFactory = $repositoryFactory;
    }

    public function impersonate(Request $request, int $id): RedirectResponse|JsonResponse
    {
        $result = ($this->impersonateUserAction)($id);
        if (! $result) {
            $request->session()
                ->flash('flash:warning', "Unable to impersonate [$id].");

            return Redirect::action($this->actionArray('index'));
        }
        $request->session()->put('impersonating', $result->id);

        return $request->wantsJson() ? new JsonResponse([], 204) : Redirect::to($result->redirectTo);
    }

    public function provision(Request $request, string $id): RedirectResponse|JsonResponse
    {
        $socialiteUser = $this->externalUserRepositoryFactory->find($id);
        $user = ($this->upsertSocialiteUserAction)($socialiteUser);

        return $request->wantsJson() ? new JsonResponse(['id' => $user->id], // @phpstan-ignore property.notFound
            204) : Redirect::action($this->actionArray('show'), ['id' => $user->id]); // @phpstan-ignore property.notFound
    }

    public function search(Request $request): View
    {
        $filter = $this->getFilterForSearchFromRequest($request);
        $count = 25;
        $vm = new FilteredViewModel($filter, $this->externalUserRepositoryFactory->filtered($filter, $count));

        return $this->makeView($this->getViewName('search'))
            ->with('viewModel', $vm)
            ->with('maxCount', $count);
    }

    protected function getFilterForSearchFromRequest(Request $request): QueryStringFilter
    {
        return \Smorken\QueryStringFilter\QueryStringFilter::from($request)
            ->setFilters(['username', 'firstName', 'lastName']);
    }

    protected function getFilterFromRequest(Request $request): QueryStringFilter
    {
        return \Smorken\QueryStringFilter\QueryStringFilter::from($request)
            ->setFilters(['userId', 'username', 'firstName', 'lastName', 'role'])
            ->addKeyValue('page');
    }

    protected function getUpsertableForUpsert(Request $request, int|string|null $id): Upsertable
    {
        return \Smorken\Domain\Actions\Upsertable::fromRequest($request, $id ?? $request->input('id'));
    }
}
