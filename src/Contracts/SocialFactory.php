<?php

declare(strict_types=1);

namespace Smorken\SocialAuth\Contracts;

use Laravel\Socialite\Contracts\Provider;

interface SocialFactory
{
    public function name(): string;

    public function provider(): Provider;
}
