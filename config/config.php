<?php

declare(strict_types=1);

return [
    'layout' => 'layouts.app',
    'debug' => env('APP_DEBUG', false),
    'cache_ttl' => 60 * 60,
    'load_admin_routes' => env('SOCIALAUTH_ADMIN_ROUTES', true),
    'load_auth_routes' => env('SOCIALAUTH_AUTH_ROUTES', true),
    'admin_route_prefix' => 'admin',
    'admin_route_middleware' => ['web', 'auth', 'can:role-admin'],
    'auth_route_prefix' => null,
    'auth_route_middleware' => ['web'],
    'provider' => env('SOCIALAUTH_PROVIDER', 'azure-latest'),
    'user_validator' => env('SOCIALAUTH_USER_VALIDATOR', \Smorken\SocialAuth\Callback\Validation\ValidateMeidInUpn::class),
    'attribute_map_class' => env('SOCIALAUTH_ATTRIBUTE_MAP_CLASS',
        \Smorken\SocialAuth\Callback\Attributes\Maps\AzureMccdMap::class),
    'controllers' => [
        'redirect' => \Smorken\SocialAuth\Http\Controllers\Auth\RedirectController::class,
        'callback' => \Smorken\SocialAuth\Http\Controllers\Auth\CallbackController::class,
        'login' => \Smorken\SocialAuth\Http\Controllers\Auth\LoginController::class,
        'logout' => \Smorken\SocialAuth\Http\Controllers\Auth\LogoutController::class,
        'help' => \Smorken\SocialAuth\Http\Controllers\Auth\HelpController::class,
        'admin' => \Smorken\SocialAuth\Http\Controllers\Admin\Controller::class,
    ],
    'repositories' => [
        \Smorken\SocialAuth\Admin\Contracts\Repositories\FilteredUsersRepository::class => \Smorken\SocialAuth\Admin\Repositories\FilteredUsersRepository::class,
        \Smorken\SocialAuth\Admin\Contracts\Repositories\FindUserRepository::class => \Smorken\SocialAuth\Admin\Repositories\FindUserRepository::class,
        \Smorken\SocialAuth\Admin\External\Contracts\Repositories\FilteredUsersRepository::class => \Smorken\SocialAuth\Admin\External\Repositories\MsGraph\FilteredUsersRepository::class,
        \Smorken\SocialAuth\Admin\External\Contracts\Repositories\FindUserRepository::class => \Smorken\SocialAuth\Admin\External\Repositories\MsGraph\FindUserRepository::class,
    ],
    'actions' => [
        \Smorken\SocialAuth\Callback\Contracts\Actions\UpsertUserAction::class => \Smorken\SocialAuth\Callback\Actions\UpsertEloquentUserAction::class,
        \Smorken\SocialAuth\Admin\Contracts\Actions\DeleteUserAction::class => \Smorken\SocialAuth\Admin\Actions\DeleteUserAction::class,
        \Smorken\SocialAuth\Admin\Contracts\Actions\ImpersonateUserAction::class => \Smorken\SocialAuth\Admin\Actions\ImpersonateUserAction::class,
        \Smorken\SocialAuth\Admin\Contracts\Actions\UpsertUserAction::class => \Smorken\SocialAuth\Admin\Actions\UpsertUserAction::class,
    ],
];
