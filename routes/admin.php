<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Smorken\SocialAuth\Http\Controllers\Admin\Controller;

Route::middleware(Config::get('sm-social-auth.admin_route_middleware', ['web', 'auth', 'can:role-admin']))
    ->prefix(Config::get('sm-social-auth.admin_route_prefix', 'admin'))
    ->group(function () {
        $controller = Config::get('sm-social-auth.controllers.admin', Controller::class);
        Route::get('user/{id}/provision', [$controller, 'provision']);
        Route::get('user/search', [$controller, 'search']);
        Route::get('user/{id}/confirm-delete', [$controller, 'confirmDelete']);
        Route::post('user/{id}/impersonate', [$controller, 'impersonate'])->name('user.impersonate');
        Route::resource('user', $controller)
            ->parameters(['user' => 'id']);
    });
