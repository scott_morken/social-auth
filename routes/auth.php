<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Smorken\SocialAuth\Http\Controllers\Auth\CallbackController;
use Smorken\SocialAuth\Http\Controllers\Auth\HelpController;
use Smorken\SocialAuth\Http\Controllers\Auth\LoginController;
use Smorken\SocialAuth\Http\Controllers\Auth\LogoutController;
use Smorken\SocialAuth\Http\Controllers\Auth\RedirectController;

Route::group([
    'prefix' => Config::get('sm-social-auth.auth_route_prefix'),
    'middleware' => Config::get('sm-social-auth.auth_route_middleware', ['web']),
], function () {
    $loginController = Config::get('sm-social-auth.controllers.login', LoginController::class);
    Route::get('login', $loginController)->name('login');

    $logoutController = Config::get('sm-social-auth.controllers.logout', LogoutController::class);
    Route::get('logout/endpoint', [$logoutController, 'endpoint'])->name('logout.endpoint');
    Route::post('logout', $logoutController)->name('logout');

    $helpController = Config::get('sm-social-auth.controllers.help', HelpController::class);
    Route::get('help', $helpController)->name('login.help');

    $redirectController = Config::get('sm-social-auth.controllers.redirect', RedirectController::class);
    Route::post('/auth/redirect', $redirectController)->name('auth.redirect');

    $callbackController = Config::get('sm-social-auth.controllers.callback', CallbackController::class);
    Route::get('/auth/callback', $callbackController)->name('auth.callback');
});
